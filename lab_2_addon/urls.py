from django.conf.urls import url, include
import lab_1.urls as lab_1
import lab_2.urls as lab_2
from .views import index

#url for app, add your URL Configuration

urlpatterns = [
	url(r'^lab-1/', include(lab_1, namespace='lab-1')),
	url(r'^lab-2/', include(lab_2, namespace='lab-2')),
	url(r'^$', index, name='index'),
]
